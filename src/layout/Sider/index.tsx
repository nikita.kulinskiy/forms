import React from 'react';
import {Layout} from 'antd';

const {Header} = Layout;

const Sider = () => {
    return <Header />;
};

export default Sider;
