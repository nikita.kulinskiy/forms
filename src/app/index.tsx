import React from 'react';
import {Breadcrumb, Layout} from 'antd';

import FirstMenu from '../layout/Header';
import SecondMenu from '../layout/Sider';
import DeltaExpress from '../pages/DeltaExpress';

import './app.scss';

const {Content} = Layout;

const App = () => {
    return (
        <Layout>
            <FirstMenu />
            <Layout className="layout">
                <SecondMenu />
                <Content className="layout__content_margin">
                    <Breadcrumb>
                        <Breadcrumb.Item>Companies</Breadcrumb.Item>
                        <Breadcrumb.Item>Delta Express</Breadcrumb.Item>
                    </Breadcrumb>
                    <DeltaExpress />
                </Content>
            </Layout>
        </Layout>
    );
};

export default App;
