import React from 'react';
import Form from './components/Form';
import Data from './components/Data';

import './main.scss';

import useStateEdits from './hooks/useStateEdits';

function Main() {
    const edits = useStateEdits();
    if (edits) {
        return <Form />;
    }
    return <Data />;
}

export default Main;
