import React from 'react';
import {Button, Typography} from 'antd';
import {EditOutlined} from '@ant-design/icons';

import InfoBlock from './components/InfoBlock';
import useEditsAction from '../../hooks/useEditsActions';
import useStateBlank from '../../hooks/useStateBlank';

const {Title, Text} = Typography;

const Data = () => {
    const {edits} = useEditsAction();
    const blank = useStateBlank();
    const edit = () => {
        edits(true);
    };

    return (
        <div className="blank">
            <div className="blank__container">
                <Title level={3}>Delta Express</Title>
                <Text className="info-block__value_color">tms-de.rhinocodes.com</Text>
                <div className="blank__buttons-group_float_right">
                    <Button onClick={edit} type="link">
                        <EditOutlined style={{fontSize: '24px', color: '#08c'}} />
                    </Button>
                </div>
            </div>
            <div className="blank__container blank__container_margin">
                <Text className="blank__text">General Info</Text>
                <InfoBlock label="Prefix:" value={blank.prefix} />
                <InfoBlock label="Phone:" value={blank.phone} />
                <InfoBlock label="Fax:" value={blank.fax} />
                <InfoBlock styles="info-block__value_color" label="Email:" value={blank.email} />
                <InfoBlock styles="info-block__value_color" label="Website:" value={blank.website} />
                <br />
                <Text className="blank__text">Identifies Numbers</Text>
                <InfoBlock label="MC#:" value={blank['mc#']} />
                <InfoBlock label="DOT#:" value={blank['dot#']} />
                <InfoBlock label="SCAC:" value={blank.scac} />
                <br />
                <Text className="blank__text">Physical Address</Text>
                <InfoBlock label="full address:" value={blank.full_address} />
                <InfoBlock label="details:" value={blank.details} />
                <br />
                <Text className="blank__text">Billing Addresses</Text>
                {blank.billing_addresses[0] ? (
                    <>
                        <InfoBlock label="address #1:" value={blank.billing_addresses[0]['address_#1']} />
                        <InfoBlock label="details:" value={blank.billing_addresses[0].details} />
                    </>
                ) : null}

                {blank.billing_addresses[1] ? (
                    <>
                        <InfoBlock label="address #2:" value={blank.billing_addresses[1]['address_#2']} />
                        <InfoBlock label="details:" value={blank.billing_addresses[1].details} />
                    </>
                ) : null}
                <br />
                {blank.factoring_company ? (
                    <div className="blank__container_color">
                        <InfoBlock label="name:" value={blank.factoring_company.name} />
                        <br />
                        <Text className="blank__text">Physical Address</Text>
                        <InfoBlock label="address:" value={blank.factoring_company.address} />
                        <InfoBlock label="details:" value={blank.factoring_company.details} />
                        <br />
                        <Text className="blank__text">Billing Addresses</Text>
                        {blank.factoring_company.billing_addresses?.length ? (
                            <>
                                <InfoBlock
                                    label="address #1:"
                                    value={blank.factoring_company.billing_addresses[0]['address_#1']}
                                />
                                <InfoBlock
                                    label="details:"
                                    value={blank.factoring_company.billing_addresses[0].details}
                                />
                                {blank.factoring_company.billing_addresses[1] ? (
                                    <>
                                        <InfoBlock
                                            label="address #2:"
                                            value={blank.factoring_company.billing_addresses[1]['address_#2']}
                                        />
                                        <InfoBlock
                                            label="details:"
                                            /* eslint-disable-next-line max-len */
                                            value={blank.factoring_company.billing_addresses[1].details}
                                        />
                                    </>
                                ) : null}
                            </>
                        ) : null}
                    </div>
                ) : null}
                <br />
                <div className="blank__container_color">
                    <Text className="blank__text">Provider</Text>
                    <InfoBlock label="Company:" value={blank.company} />
                    <InfoBlock label="Key:" value={blank.key} />
                </div>
            </div>
        </div>
    );
};

export default Data;
