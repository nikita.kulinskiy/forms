import React from 'react';
import cn from 'classnames';

const InfoBlock = ({label, value, styles = []}: any) => {
    return (
        <div className="info-block">
            <div className="info-block__label">{label}</div>
            <div className={cn('info-block__value', styles)}>{value}</div>
        </div>
    );
};

export default InfoBlock;
