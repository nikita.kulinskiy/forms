import React, {useState, useEffect} from 'react';
import {Form, Input} from 'antd';

const AddAddress = ({fields, add, rules = []}: any) => {
    const [camAdd, setCanAdd] = useState(true);
    useEffect(() => {
        if (fields.length > 1) {
            setCanAdd(false);
        }
    });
    return (
        <>
            {fields.map((field: {key: string | number | null | undefined; name: number}) => {
                return (
                    <div key={field.key}>
                        <Form.Item
                            rules={rules}
                            name={[field.name, `address_#${field.name + 1}`]}
                            label={`address #${field.name + 1}`}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item rules={rules} name={[field.name, 'details']} label="details">
                            <Input />
                        </Form.Item>
                    </div>
                );
            })}
            {/* eslint-disable-next-line no-magic-numbers */}
            {camAdd ? (
                <p>
                    <a
                        className="data-form__a_margin"
                        onClick={() => {
                            add();
                        }}
                    >
                        + add billing address
                    </a>
                </p>
            ) : null}
        </>
    );
};

export default AddAddress;
