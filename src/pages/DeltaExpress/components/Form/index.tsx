import React, {useState, useEffect} from 'react';
import {Form, Input, Button, Checkbox, Select, Typography} from 'antd';
import MaskedInput from 'antd-mask-input';

import AddAddress from './components/AddAddress';
import useBlankAction from '../../hooks/useBlankAction';
import useEditsAction from '../../hooks/useEditsActions';
import useStateBlank from '../../hooks/useStateBlank';
import FactoringCompany from './components/FactoringCompany';

const {Option} = Select;
const {Title, Text} = Typography;

const DeltaExpressForm = () => {
    const blankData = useStateBlank();
    const {saveBlank} = useBlankAction();
    const {edits} = useEditsAction();
    const [showFactoringCompany, setShowFactoringCompany] = useState(false);
    const layout = {
        labelCol: {span: 5},
        wrapperCol: {span: 25},
    };
    async function onFinish(values: any) {
        const copyValue = JSON.parse(JSON.stringify(values));
        await saveBlank(copyValue);
        edits(false);
    }
    const rules = [{required: true, message: 'Not filled!'}];
    const cancel = () => {
        edits(false);
    };

    useEffect(() => {
        if (blankData.factoring_company) {
            setShowFactoringCompany(true);
        }
    }, []);

    return (
        <Form
            {...layout}
            name="delta_express"
            onFinish={onFinish}
            labelAlign="left"
            className="blank"
            initialValues={blankData}
        >
            <div className="blank__container">
                <Title level={3}>Delta Express</Title>
                <Text className="blank__text_color">tms-de.rhinocodes.com</Text>
                <div className="blank__buttons-group_float">
                    <Button type="link" htmlType="submit">
                        Save
                    </Button>
                    <Button onClick={cancel} type="link">
                        Cancel
                    </Button>
                </div>
            </div>
            <div className="blank__container blank__container_margin">
                <Text className="blank__text blank__star">Company Name</Text>
                <Form.Item rules={rules} name={['company_name']}>
                    <Input />
                </Form.Item>
                <Text className="blank__text">General Info</Text>
                <Form.Item rules={rules} name={['prefix']} label="Prefix">
                    <Input className="blank__input_size_s" />
                </Form.Item>
                <Form.Item rules={rules} name={['phone']} label="Phone">
                    <MaskedInput mask="+380111111111" className="blank__input_size_m" />
                </Form.Item>
                <Form.Item name={['fax']} label="Fax">
                    <MaskedInput mask="+380111111111" className="blank__input_size_m" />
                </Form.Item>
                <Form.Item name={['email']} label="Email">
                    <Input type="email" />
                </Form.Item>
                <Form.Item name={['website']} label="Website">
                    <Input />
                </Form.Item>
                <Text className="blank__text">Identifies Numbers</Text>
                <Form.Item name={['mc#']} label="MC#">
                    <Input className="blank__input_size_m" />
                </Form.Item>
                <Form.Item name={['dot#']} label="DOT#">
                    <Input className="blank__input_size_m" />
                </Form.Item>
                <Form.Item name={['scac']} label="SCAC">
                    <Input className="blank__input_size_m" />
                </Form.Item>
                <Text className="blank__text blank__star">Physical Address</Text>
                <Form.Item rules={rules} name={['full_address']} label="full address">
                    <Input />
                </Form.Item>
                <Form.Item rules={rules} name={['details']} label="details">
                    <Input />
                </Form.Item>
                <Text className="blank__text blank__star">Billing Addresses</Text>
                <Form.List name="billing_addresses">
                    {(fields, {add}) => (
                        // eslint-disable-next-line max-len
                        <AddAddress fields={fields} add={add} rules={rules} />
                    )}
                </Form.List>
                <Form.Item>
                    <Checkbox
                        onChange={() => setShowFactoringCompany(!showFactoringCompany)}
                        checked={showFactoringCompany}
                    >
                        <Text className="blank__text">Factoring Company</Text>
                    </Checkbox>
                </Form.Item>
                {/* eslint-disable-next-line max-len */}
                {showFactoringCompany ? <FactoringCompany rules={rules} /> : null}
                <div className="blank__container_color">
                    <Text className="blank__text blank__star">Provider</Text>
                    <Form.Item rules={rules} name={['company']} label="Company">
                        <Select placeholder="Please select a company">
                            <Option value="Trimble">Trimble</Option>
                            <Option value="MapQuest">MapQuest</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item rules={rules} name={['key']} label="Key">
                        <Input />
                    </Form.Item>
                </div>
            </div>
        </Form>
    );
};

export default DeltaExpressForm;
