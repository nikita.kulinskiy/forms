import * as loaderTypes from '../actionsTypes';

const initialState = {
    blank: {
        billing_addresses: [
            {
                details: '',
                'address_#1': '',
            },
        ],
        factoring_company: {
            billing_addresses: [
                {
                    details: '',
                    'address_#1': '',
                },
            ],
        },
    },
    edits: true,
};

export const reducer = (state = initialState, action: {type: any; blank: any; edits: any}) => {
    switch (action.type) {
        case loaderTypes.SAVE_BLANK:
            return {
                ...state,
                blank: action.blank,
            };
        case loaderTypes.EDITS:
            return {
                ...state,
                edits: action.edits,
            };
        default:
            return state;
    }
};

export default reducer;
