import {useDispatch} from 'react-redux';
import {saveBlank} from '../actions';

export default function useBlankAction() {
    const dispatch = useDispatch();
    return {
        saveBlank: (values: any) => dispatch(saveBlank(values)),
    };
}
