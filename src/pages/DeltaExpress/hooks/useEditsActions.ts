import {useDispatch} from 'react-redux';
import {edits} from '../actions';

export default function useEditsAction() {
    const dispatch = useDispatch();
    return {
        edits: (values: any) => dispatch(edits(values)),
    };
}
