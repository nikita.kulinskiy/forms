import {useSelector} from 'react-redux';

export default function useStateBlank() {
    return useSelector((state: {blank: any}) => state.blank);
}
