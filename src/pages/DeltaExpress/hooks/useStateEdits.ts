import {useSelector} from 'react-redux';

export default function useStateEdits() {
    return useSelector((state: {edits: any}) => state.edits);
}
