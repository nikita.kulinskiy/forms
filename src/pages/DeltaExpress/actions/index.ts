import * as loaderTypes from '../actionsTypes';

export function saveBlank(blankData: any) {
    return (dispatch: any) => {
        dispatch({
            type: loaderTypes.SAVE_BLANK,
            blank: blankData,
        });
    };
}

export function edits(value: any) {
    return (dispatch: any) => {
        dispatch({
            type: loaderTypes.EDITS,
            edits: value,
        });
    };
}
